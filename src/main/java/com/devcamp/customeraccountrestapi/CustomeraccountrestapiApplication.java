package com.devcamp.customeraccountrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomeraccountrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomeraccountrestapiApplication.class, args);
	}

}
