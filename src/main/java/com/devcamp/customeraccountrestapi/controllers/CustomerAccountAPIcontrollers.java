package com.devcamp.customeraccountrestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountrestapi.models.Account;
import com.devcamp.customeraccountrestapi.models.Customer;

import org.springframework.web.bind.annotation.GetMapping;


@RestController
@RequestMapping("/")
@CrossOrigin
public class CustomerAccountAPIcontrollers {
    @GetMapping("/accounts")
    public ArrayList<Account> getAccounts(){
        Customer customer1 = new Customer(0,"an",0);
        Customer customer2 = new Customer(1,"minh",10);
        Customer customer3 = new Customer(1,"my",10);

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);


        Account account1 = new Account(0, customer1, 1000000);
        Account account2 = new Account(1, customer2,50000);
        Account account3 = new Account(2, customer3,50000);

        ArrayList<Account> arrListAccount = new ArrayList<Account>();

        arrListAccount.add(account1);
        arrListAccount.add(account2);
        arrListAccount.add(account3);

        return arrListAccount;
    }
    
}
